﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GGJ2020
{
    public class SplashScreen : MonoBehaviour
    {
        private void Start()
        {
            StartCoroutine(WaitAndStart());
        }

        private IEnumerator WaitAndStart()
        {
            yield return new WaitForSeconds(3);

            SceneManager.LoadScene("Splash2");
        }
    }
}