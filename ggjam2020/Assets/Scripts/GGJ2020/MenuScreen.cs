﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GGJ2020
{
    public class MenuScreen : MonoBehaviour
    {
        [SerializeField] private CreditsScreen credits;

        private void Start()
        {
            MusicLoop.Instance.PlayMusicLoop("tema-completo");
        }
        
        public void OnStartClick()
        {
            SceneManager.LoadScene("Game");
        }

        public void OnCreditsClick()
        {
            gameObject.SetActive(false);
            credits.gameObject.SetActive(true);
        }

        public void OnCloseClick()
        {
            Application.Quit();
        }
    }
}