﻿using GGJ2020.Data;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ2020
{
    public class PersonView : MonoBehaviour
    {
        [SerializeField] private Image _picture;
        [SerializeField] private PersonDialogue _dialogue;
        [SerializeField] private PersonFeelings _feelings;
        [SerializeField] private Text _name;
        [SerializeField] private Text _avatarId;

        private Person _personData;
        private AudioPlayer _audioPlayer;
        private int _currentEmotion = 1;

        public string Name
        {
            get { return _personData.Name; }
        }

        public void Initialize(Person personData, float currentValue)
        {
            _personData = personData;
            
            _audioPlayer = GetComponent<AudioPlayer>();
            
            _name.text = _personData.Name;
            _avatarId.text = string.Format("{0}{1}", _personData.Gender, _personData.AvatarId);
            _dialogue.gameObject.SetActive(false);
            _feelings.gameObject.SetActive(false);
            
            ShowEmotion(currentValue);
        }
        
        public void ShowEmotion(float currentValue)
        {
            if (currentValue <= -2)
                _currentEmotion = 0;
            else if (currentValue > -2 && currentValue < 4)
                _currentEmotion = 1;
            else if (currentValue >= 4)
                _currentEmotion = 2;

            var characterImg = string.Format("Characters/{0}_{1}_{2}", 
                _personData.Gender == Person.Genders.MALE ? "male" : "female", 
                _personData.AvatarId,
                GetSentimentString());
            _picture.sprite = Resources.Load <Sprite>(characterImg);
        }
        
        public void Speak(string responseText)
        {
            _feelings.Hide();
            _dialogue.Show(responseText);

            _audioPlayer.PlaySound(CurrentSpeakSound());
        }

        private string CurrentSpeakSound()
        {
            return string.Format("{0}_{1}_{2}",
                _personData.Gender == Person.Genders.MALE ? "male" : "female",
                _personData.AvatarId,
                GetSentimentString());
        }

        private string GetSentimentString()
        {
            if (_currentEmotion == 0)
                return "sad";
            else if (_currentEmotion == 2)
                return "happy";

            return "meh";
        }

        public void Feel(float questionResultHeartModif, float questionResultBrainModif, float questionResultEgoModif)
        {
            _feelings.Show(questionResultHeartModif, questionResultBrainModif, questionResultEgoModif);
            _dialogue.Hide();
        }

        public void JustListen()
        {
            _feelings.Hide();        
            _dialogue.Hide();
        }
    }
}