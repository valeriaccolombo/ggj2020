namespace GGJ2020
{
    public class Configs
    {
        public static int MaxPoints = 10;
        public static int MinPoints = -10;
        public static int MaxRounds = 8;
    }
}