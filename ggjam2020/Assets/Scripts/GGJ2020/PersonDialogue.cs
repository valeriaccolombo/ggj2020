using UnityEngine;
using UnityEngine.UI;

namespace GGJ2020
{
    public class PersonDialogue : MonoBehaviour
    {
        [SerializeField] private GameObject _parent;
        [SerializeField] private Text _text;
        
        public void Show(string responseText)
        {
            _text.text = responseText;
            _parent.SetActive(true);
            gameObject.SetActive(true);
        }
        
        public void Hide()
        {
            _parent.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}