﻿using UnityEngine;

namespace GGJ2020
{
    public class CreditsScreen : MonoBehaviour
    {
        [SerializeField] private MenuScreen menu;

        public void OnCloseClick()
        {
            gameObject.SetActive(false);
            menu.gameObject.SetActive(true);
        }
    }
}