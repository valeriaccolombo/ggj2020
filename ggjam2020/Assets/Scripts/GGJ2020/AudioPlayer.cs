﻿using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private AudioSource _audioSource;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(string sound)
    {
        _audioSource.clip = Resources.Load<AudioClip>("Sound/"+sound);
        _audioSource.loop = false;
        _audioSource.Play();
    }

    public void PlayDefault()
    {
        _audioSource.loop = false;
        _audioSource.Play();
    }
    
    public void PlayMusic(string music)
    {
        _audioSource.clip = Resources.Load<AudioClip>("Sound/"+music);
        _audioSource.loop = true;
        _audioSource.Play();
    }
}
