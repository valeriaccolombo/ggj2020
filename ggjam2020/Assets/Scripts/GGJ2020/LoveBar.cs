﻿using UnityEngine;
using UnityEngine.UI;

namespace GGJ2020
{
    public class LoveBar : MonoBehaviour
    {
        [SerializeField] private Slider leftSlider;
        [SerializeField] private Slider rightSlider;
        [SerializeField] private Slider totalSlider;
        [SerializeField] private Text value;
        
        public void UpdateStatus(float currentPoints)
        {
            var realScale = Configs.MaxPoints - Configs.MinPoints;
            var realCurrent = currentPoints + realScale / 2f;

            var barsVal = realCurrent / realScale;

            value.text = currentPoints.ToString();
            leftSlider.value = 1 - barsVal;
            rightSlider.value = 1 - barsVal;
            totalSlider.value = barsVal;
        }
    }
}