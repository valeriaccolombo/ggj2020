﻿using System;
using System.Collections.Generic;
using GGJ2020.Data;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Psychologist : MonoBehaviour
{
    [SerializeField] private Toggle _question1;
    [SerializeField] private Toggle _question2;
    [SerializeField] private Toggle _question3;
    [SerializeField] private Toggle _question4;
    
    [SerializeField] private Text _question1Text;
    [SerializeField] private Text _question2Text;
    [SerializeField] private Text _question3Text;
    [SerializeField] private Text _question4Text;

    [SerializeField] private Animator _notebookAnimator;

    [SerializeField] private Text _pacientNamesText;
    private AudioPlayer _audioPlayer;

    private Action<QuestionData> _onQuestionSelected;
    private List<QuestionData> _questions;

    private bool _waitDontListentToTheToggles;

    private void Awake()
    {
        _audioPlayer = GetComponent<AudioPlayer>();
    }

    public void SetPacientNames(string nameA, string nameB)
    {
        _pacientNamesText.text = String.Format("{0} y {1}", nameA, nameB);
    }
    
    public void ShowQuestions(List<QuestionData> questions, Action<QuestionData> onQuestionSelected)
    {
        _waitDontListentToTheToggles = true;
        
        _question1.isOn = false;
        _question2.isOn = false;
        _question3.isOn = false;
        _question4.isOn = false;

        _questions = questions;
        _question1Text.text = _questions[0].Text;
        _question2Text.text = _questions[1].Text;
        _question3Text.text = _questions[2].Text;
        _question4Text.text = _questions[3].Text;

        _onQuestionSelected = onQuestionSelected;
        
        _notebookAnimator.Play("NotebookOpen");

        _waitDontListentToTheToggles = false;
        
        Speak();
    }

    public void OnToggleValueChanged()
    {
        if (_waitDontListentToTheToggles)
            return;
        
        var selectedQuestion = 0;
        
        if (_question1.isOn)
        {
            selectedQuestion = 0;
        }
        else if (_question2.isOn)
        {
            selectedQuestion = 1;
        }
        else if (_question3.isOn)
        {
            selectedQuestion = 2;
        }
        else if (_question4.isOn)
        {
            selectedQuestion = 3;
        }

        var animName = string.Format("NotebookSelected{0}", selectedQuestion + 1);
        _notebookAnimator.Play(animName);
        _onQuestionSelected(_questions[selectedQuestion]);

        _audioPlayer.PlaySound("pencil");
    }

    public void Speak()
    {
        var randomSound = string.Format("psico{0}", Random.Range(1,6));
        _audioPlayer.PlaySound(randomSound);
    }
}
