using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

namespace GGJ2020.Data
{
    public class Person
    {
        public enum Genders { MALE, FEMALE }

        public string Name { get; private set; }
        public Genders Gender { get; private set; }
        public int AvatarId { get; private set; }

        private Random _random = new Random();

        public Person()
        {
            CreatePerson();
        }

        public Person(string otherPersonName, Genders otherPersonGender, int otherPersonAvatarId)
        {
            CreatePerson();
            while ((otherPersonGender == Gender && otherPersonAvatarId == AvatarId) || (otherPersonName == Name))
            {
                CreatePerson();
            }
        }
        
        private void CreatePerson()
        {
            if (_random.Next(0, 2) >= 1)
            {
                Gender =  Genders.FEMALE;
                Name = Names.FemaleNames[_random.Next(0, Names.FemaleNames.Count)];
                AvatarId = _random.Next(0, 2);
            }
            else
            {
                Gender =  Genders.MALE;
                Name = Names.MaleNames[_random.Next(0, Names.MaleNames.Count)];
                AvatarId = _random.Next(0, 2);
            }
        }
    }
}