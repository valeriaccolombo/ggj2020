namespace GGJ2020.Data
{
    [System.Serializable]
    public class CoupleData
    {
        public int Id;
        public string StartingPoints;
        public float Heart;
        public float Brain;
        public float Ego;
        public string Intro1;
        public string Intro2;
    }
}