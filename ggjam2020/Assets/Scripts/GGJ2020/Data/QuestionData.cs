namespace GGJ2020.Data
{
    [System.Serializable]
    public class QuestionData
    {
        public int Id;
        public string Text;
        public float Heart;
        public float Brain;
        public float Ego;
    }
}