using System.Collections.Generic;

namespace GGJ2020.Data
{
    public class Names
    {
        public static List<string> FemaleNames = new List<string> { "Mara", "Vale", "Harmonia", "Lotte", "Jane", "Simone", "Aurora", "Nora", "Marimar", "Meche", "Luna", "Soraya", "Ayelén", "Sam", "René" };
        public static List<string> MaleNames = new List<string> { "Eze", "Fabri", "Sam", "René", "Bernardo", "Roberto", "Johann", "Cadmo", "Werther", "Frank", "Adri", "Guido", "Fitz", "Luis Fernando", "Nahuel" };
    }
}
