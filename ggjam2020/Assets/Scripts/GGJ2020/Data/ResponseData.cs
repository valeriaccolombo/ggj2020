namespace GGJ2020.Data
{
    [System.Serializable]
    public class ResponseData
    {
        public int IdCouple;
        public int IdQuestion;
        public string Text1;
        public string Text2;

        public ResponseData(int coupleId, int questionId, string text1, string text2)
        {
            IdCouple = coupleId;
            IdQuestion = questionId;
            Text1 = text1;
            Text2 = text2;
        }
    }
}