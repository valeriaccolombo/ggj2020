using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GGJ2020.Data
{
    public class DataLoader
    {
        public List<CoupleData> CouplesData { get; private set; }
        public List<QuestionData> QuestionsData { get; private set; }
        public List<ResponseData> ResponsesData { get; private set; }

        public void LoadData()
        {
            var couplesFile = Resources.Load<TextAsset>("Data/couples");
            var couplesRawData = JsonUtility.FromJson<CoupleListData>(couplesFile.text);
            CouplesData = couplesRawData.couples.ToList();

            var questionsFile = Resources.Load<TextAsset>("Data/questions");
            var questionsRawData = JsonUtility.FromJson<QuestionsListData>(questionsFile.text);
            QuestionsData = questionsRawData.questions.ToList();

            var responsesFile = Resources.Load<TextAsset>("Data/responses");
            var responsesRawData = JsonUtility.FromJson<ResponsesListData>(responsesFile.text);
            ResponsesData = responsesRawData.responses.ToList();
        }
    }
}