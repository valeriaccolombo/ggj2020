namespace GGJ2020.Data
{
    [System.Serializable]
    public class ResponsesListData
    {
        public ResponseData[] responses;
    }
}