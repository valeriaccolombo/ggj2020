namespace GGJ2020.Data
{
    [System.Serializable]
    public class QuestionsListData
    {
        public QuestionData[] questions;
    }
}