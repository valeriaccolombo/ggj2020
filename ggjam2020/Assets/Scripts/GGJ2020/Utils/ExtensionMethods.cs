using System;
using System.Collections.Generic;
using System.Linq;

namespace GGJ2020.Utils
{
    public static class ExtensionMethods
    {
        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.PickRandom(1).Single();
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }
			
        public static bool ContainsAll<A>(this IEnumerable<A> l1, IEnumerable<A> l2) {
            return l2.All(l1.Contains);
        }
    }
}