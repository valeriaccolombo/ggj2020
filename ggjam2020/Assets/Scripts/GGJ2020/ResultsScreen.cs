using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GGJ2020
{
    public class ResultsScreen : MonoBehaviour
    {
        [SerializeField] private Text _resultsText;
        [SerializeField] private Text _historyText;
        [SerializeField] private Slider totalSlider;

        public void Show(TherapySession therapySession)
        {
            gameObject.SetActive(true);

            _resultsText.text = therapySession.CurrentPoints.ToString("#.##");

            _historyText.text = GetFinalHistory(therapySession.CurrentPoints);
            
            var realScale = Configs.MaxPoints - Configs.MinPoints;
            var realCurrent = therapySession.CurrentPoints + realScale / 2f;
            totalSlider.value = realCurrent / realScale;
        }

        private string GetFinalHistory(float currentPoints)
        {
            if(currentPoints < -8)
                return "Resentida. La pareja te apuñalará a la salida del consultorio.";
            if (currentPoints < -5)
                return "Mala terapia. Unas semanas después encontrarás tu auto todo rayado.";
            if (currentPoints < -2)
                return "Y bue. Estaban mal y no pudiste ayudarlos.";
            if (currentPoints < 0)
                return "Matrimonio standard. Siguen juntos pero son infelices.";
            if (currentPoints == 0)
                return "Neutral. Tu paso por su vida no significó nada.";
            if (currentPoints < 2)
                return "Mejora marginal. Aunque se preguntan si está bien seguir.";
            if (currentPoints < 5)
                return "Nuevos horizontes. Siguen, pero probarán el poliamor.";
            if (currentPoints < 8)
                return "Felicidad standard. Bien, consolidaste una típica pareja burguesa.";
            else
                return "Wena, Dra. Freud. Te invitarán a un asado de perdices pues son felices.";
        }

        public void OnBackToMenuClick()
        {
            StartCoroutine(WaitAndExit());
        }

        private IEnumerator WaitAndExit()
        {
            yield return new WaitForSeconds(0.5f);
            SceneManager.LoadScene("Menu");
        }
    }
}