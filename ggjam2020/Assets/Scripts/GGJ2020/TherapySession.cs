using System.Collections.Generic;
using System.Linq;
using GGJ2020.Data;
using GGJ2020.Utils;

namespace GGJ2020
{
    public class TherapySession
    {
        private List<QuestionData> _questions;
        private List<ResponseData> _responses;

        public CoupleData Couple { get; private set; }
        public Person PersonA { get; private set; }
        public Person PersonB { get; private set; }
        public float CurrentPoints { get; private set; }
        public int Rounds { get; private set; }

        public TherapySession(List<QuestionData> questions, List<ResponseData> responses, CoupleData couple)
        {
            _questions = questions.GetRange(0, questions.Count);
            _responses = responses;
            Couple = couple;
            
            PersonA = new Person();
            PersonB = new Person(PersonA.Name, PersonA.Gender, PersonA.AvatarId);

            Rounds = 0;
        }

        public List<QuestionData> GetPosibleQuestions(int count)
        {
            return _questions.PickRandom(count).ToList();
        }

        public QuestionResult MakeQuestion(QuestionData question)
        {
            Rounds++;

            _questions.Remove(question);
            
            var heartModif = Couple.Heart * question.Heart;
            var brainModif = Couple.Brain * question.Brain;
            var egoModif = Couple.Ego * question.Ego;
            
            CurrentPoints += heartModif + brainModif + egoModif;
            if (CurrentPoints > Configs.MaxPoints)
                CurrentPoints = Configs.MaxPoints;
            if (CurrentPoints < Configs.MinPoints)
                CurrentPoints = Configs.MinPoints;
            
            var response = _responses.Find(r => (r.IdCouple == Couple.Id) && (r.IdQuestion == question.Id));
            
            if (response == null)
                response = new ResponseData(Couple.Id, question.Id, "...", "...");

            return new QuestionResult(heartModif, brainModif, egoModif, response);
        }
    }

    public class QuestionResult
    {
        public float HeartModif { get; private set; }
        public float BrainModif { get; private set; }
        public float EgoModif { get; private set; }

        public ResponseData Response { get; private set; }

        public QuestionResult(float heartModif, float brainModif, float egoModif, ResponseData response)
        {
            HeartModif = heartModif;
            BrainModif = brainModif;
            EgoModif = egoModif;
            Response = response;
        }
    }
}