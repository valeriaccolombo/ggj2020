﻿using System.Collections;
using System.Linq;
using GGJ2020.Data;
using GGJ2020.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ2020
{
    public class GameView : MonoBehaviour
    {
        [SerializeField] private Text _coupleId;
        [SerializeField] private LoveBar _loveBar;
        [SerializeField] private PersonView _personA;
        [SerializeField] private PersonView _personB;
        [SerializeField] private Psychologist _psychologist;
        [SerializeField] private ResultsScreen _resultsScreen;
        
        [SerializeField] private Animator _endSessionAnimator;
        [SerializeField] private GameObject _endSessionScreen;
        [SerializeField] private Text _endSessionScreenText;

        private AudioPlayer _gameAudioPlayer;

        private TherapySession _therapySession;

        private void Start()
        {
            MusicLoop.Instance.PlayMusicLoop("temainstrumental", 0.3f);
            _gameAudioPlayer = GetComponent<AudioPlayer>();

            gameObject.SetActive(true);
            _resultsScreen.gameObject.SetActive(false);

            var dataLoader = new DataLoader();
            dataLoader.LoadData();

            _therapySession = new TherapySession(dataLoader.QuestionsData, dataLoader.ResponsesData,
                GetRandomCouple(dataLoader));

            _coupleId.text = "Pareja #" + _therapySession.Couple.Id + " - Round #" + _therapySession.Rounds;
            _loveBar.UpdateStatus(_therapySession.CurrentPoints);
            _personA.Initialize(_therapySession.PersonA, _therapySession.CurrentPoints);
            _personB.Initialize(_therapySession.PersonB, _therapySession.CurrentPoints);
            
            _psychologist.SetPacientNames(_personA.Name, _personB.Name);
            
            StartCoroutine(WaitAndStartIntro());
        }

        private IEnumerator WaitAndStartIntro()
        {
            yield return new WaitForSeconds(1);
            
            if (!string.IsNullOrEmpty(_therapySession.Couple.Intro1))
            {
                StartCoroutine(Person1Intro());
            }
            else if (!string.IsNullOrEmpty(_therapySession.Couple.Intro2))
            {
                StartCoroutine(Person2Intro());
            }
            else
            {
                StartCoroutine(EmotionsHit());
            }
        }

        private CoupleData GetRandomCouple(DataLoader dataLoader)
        {
            return dataLoader.CouplesData.PickRandom(1).First();
        }

        private void NewRound()
        {
            if (_therapySession.Rounds < Configs.MaxRounds)
            {
                _psychologist.ShowQuestions(_therapySession.GetPosibleQuestions(4), OnQuestionSelected);
            }
            else
            {
                StartCoroutine(FinishSession());
            }
        }

        private IEnumerator FinishSession()
        {
            _endSessionScreenText.text = _endSessionScreenText.text
                .Replace("{X1}", _personA.Name)
                .Replace("{X2}", _personB.Name);
            _endSessionScreen.SetActive(true);
            _endSessionAnimator.Play("enter");
            
            yield return new WaitForSeconds(3);
            
            MusicLoop.Instance.PlayMusicLoop("tema-sin-intro");

            yield return new WaitForSeconds(8);
            
            gameObject.SetActive(false);
            _resultsScreen.Show(_therapySession);
        }

        private void OnQuestionSelected(QuestionData selectedQuestion)
        {
            StartCoroutine(WaitAndMakeQuestions(selectedQuestion));
        }
        
        private IEnumerator WaitAndMakeQuestions(QuestionData selectedQuestion)
        {
            yield return new WaitForSeconds(1f);

            var questionResult = _therapySession.MakeQuestion(selectedQuestion);
            
            _personA.ShowEmotion(_therapySession.CurrentPoints);
            _personB.ShowEmotion(_therapySession.CurrentPoints);
            _loveBar.UpdateStatus(_therapySession.CurrentPoints);
            
            if (!string.IsNullOrEmpty(questionResult.Response.Text1))
            {
                StartCoroutine(Person1Speaks(questionResult));
            }
            else if (!string.IsNullOrEmpty(questionResult.Response.Text2))
            {
                StartCoroutine(Person2Speaks(questionResult));
            }
            else
            {
                StartCoroutine(EmotionsHit());
            }
        }

        private IEnumerator Person1Intro()
        {
            _personA.Speak(_therapySession.Couple.Intro1);
            _personB.JustListen();
            
            yield return new WaitForSeconds(5);
            
            if (!string.IsNullOrEmpty(_therapySession.Couple.Intro2))
            {
                StartCoroutine(Person2Intro());
            }
            else
            {
                StartCoroutine(EmotionsHit());
            }
        }
        
        private IEnumerator Person2Intro()
        {
            _personB.Speak(_therapySession.Couple.Intro2);
            _personA.JustListen();
            
            yield return new WaitForSeconds(5);
            
            StartCoroutine(EmotionsHit());
        }
        
        private IEnumerator Person1Speaks(QuestionResult questionResult)
        {
            _personA.Speak(questionResult.Response.Text1);
            _personB.Feel(questionResult.HeartModif, questionResult.BrainModif, questionResult.EgoModif);
            
            yield return new WaitForSeconds(5);
            
            if (!string.IsNullOrEmpty(questionResult.Response.Text2))
            {
                StartCoroutine(Person2Speaks(questionResult));
            }
            else
            {
                StartCoroutine(EmotionsHit());
            }
        }

        private IEnumerator Person2Speaks(QuestionResult questionResult)
        {
            _personB.Speak(questionResult.Response.Text2);
            _personA.Feel(questionResult.HeartModif, questionResult.BrainModif, questionResult.EgoModif);
            
            yield return new WaitForSeconds(5);
            
            StartCoroutine(EmotionsHit());
        }

        private IEnumerator EmotionsHit()
        {
            _psychologist.Speak();
            
            _personA.JustListen();
            _personB.JustListen();
            
            _personA.ShowEmotion(_therapySession.CurrentPoints);
            _personB.ShowEmotion(_therapySession.CurrentPoints);
            
            _loveBar.UpdateStatus(_therapySession.CurrentPoints);
            yield return new WaitForSeconds(1);
            
            _coupleId.text = "Pareja #" + _therapySession.Couple.Id + " - Round #" + _therapySession.Rounds;
            NewRound();
        }
    }
}