﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GGJ2020
{
    public class SplashScreen2 : MonoBehaviour
    {
        private void Start()
        {
            StartCoroutine(WaitAndStart());
        }

        private IEnumerator WaitAndStart()
        {
            yield return new WaitForSeconds(4);

            SceneManager.LoadScene("Menu");
        }
    }
}