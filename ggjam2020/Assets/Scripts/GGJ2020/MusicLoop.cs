﻿using UnityEngine;

public class MusicLoop : MonoBehaviour
{
    private static MusicLoop _instance;

    public static MusicLoop Instance 
    { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            _audioSource = GetComponent<AudioSource>();
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private string _currentMusic = "tema-completo";
    private AudioSource _audioSource;
    
    public void PlayMusicLoop(string music, float volume = 1f)
    {
        if (music != _currentMusic)
        {
            _currentMusic = music;
            _audioSource.volume = volume;
            _audioSource.clip = Resources.Load<AudioClip>("Sound/"+music);
            _audioSource.loop = true;
            _audioSource.Play();
        }
    }
}
