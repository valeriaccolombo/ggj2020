using UnityEngine;
using UnityEngine.UI;

namespace GGJ2020
{
    public class PersonFeelings : MonoBehaviour
    {
        [SerializeField] private GameObject _heart;
        [SerializeField] private GameObject _brain;
        [SerializeField] private GameObject _ego;
        
        [SerializeField] private Text _heartText;
        [SerializeField] private Text _brainText;
        [SerializeField] private Text _egoText;
        
        private Color _positiveColor = new Color(106f/255f, 163f/255f, 152f/255f);
        private Color _negativeColor = new Color(187f/255f, 34f/255f, 34f/255f);
        
        public void Show(float heartModif, float brainModif, float egoModif)
        {
            _heartText.text = ResultsOf(heartModif);
            _brainText.text = ResultsOf(brainModif);
            _egoText.text = ResultsOf(egoModif);

            _heartText.color = _heartText.text.Contains("+") ? _positiveColor : _negativeColor;
            _brainText.color = _brainText.text.Contains("+") ? _positiveColor : _negativeColor;
            _egoText.color = _egoText.text.Contains("+") ? _positiveColor : _negativeColor;
                
            _heart.gameObject.SetActive(_heartText.text != "=");
            _brain.gameObject.SetActive(_brainText.text != "=");
            _ego.gameObject.SetActive(_egoText.text != "=");
            
            gameObject.SetActive(true);
        }
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }
        
        private string ResultsOf(float modif)
        {
            if (modif < -3f)
            {
                return "--";
            }
            if (modif < -0.3f)
            {
                return "-";
            }
            if (modif > 3f)
            {
                return "++";
            }
            if (modif > 0.3f)
            {
                return "+";
            }
            return "=";
        }
    }
}